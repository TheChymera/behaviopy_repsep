\documentclass{beamer}
\usepackage[orientation=portrait,size=a0]{beamerposter}
\mode<presentation>{\usetheme{ZH_AIC}}
% \usepackage{chemformula}
\usepackage[utf8]{inputenc}
\usepackage{multicol}
% \usepackage[german, english]{babel} % required for rendering German special characters
\usepackage{siunitx} %pretty measurement unit rendering
\usepackage[autoprint=false, gobble=auto, pyfuture=all]{pythontex} % create figures on-line directly from python!
\usepackage{hyperref} %enable hyperlink for urls
\usepackage{ragged2e}
\usepackage{graphviz}
\usepackage[font=scriptsize,justification=justified]{caption}
\usepackage{subfig}
\usepackage{booktabs}
\usepackage{verbatimbox}
\usepackage[noabbrev]{cleveref}

% \sisetup{per=frac,fraction=sfrac}
\sisetup{per-mode=symbol}

\definecolor{elg}{gray}{0.95}
\definecolor{vlg}{gray}{0.90}

\input{pythontex/functions.tex}
\begin{pythontexcustomcode}[begin]{py}
	DOC_STYLE = 'poster/main.conf'
	pytex.add_dependencies(
		DOC_STYLE,
		'poster/wide.conf',
		'poster/swarmplots.conf',
		'poster/matrices.conf',
		)
\end{pythontexcustomcode}

\usepackage{array,booktabs,tabularx}
\newcolumntype{Z}{>{\centering\arraybackslash}X} % centered tabularx columns

\title{BehavioPy - Python Evaluation, Analysis, and Plotting for Behaviour and Physiology}
\author{Horea-Ioan Ioanas$^{1}$, Bechara John Saab$^{2}$, Markus Rudin$^{1}$}
\institute[ETH]{$^{1}$Institute for Biomedical Engineering, ETH and University of Zurich \\ $^{2}$Preclinical Laboratory for Translational Research into Affective Disorders, DPPP, Psychiatric Hospital, University of Zurich}
\date{\today}

\newlength{\columnheight}
\setlength{\columnheight}{0.91\textheight}

\begin{document}
\begin{myverbbox}[\scriptsize]{\spcode}
from behaviopy.plotting import expandable_ttest

expandable_ttest(df,
    compare="Treatment",
    comparisons={"Period [days]":[]},
    datacolumn_label="Sucrose Preference Ratio",
    rename_treatments={
        "cFluDW":"Fluoxetine",
        "cFluDW_":"Control",
        },
    save_as="sucrosepreference_treatment.pdf",
    )
\end{myverbbox}
\begin{frame}
\begin{columns}
	\begin{column}{.4\textwidth}
		\begin{beamercolorbox}[center]{postercolumn}
			\begin{minipage}{.98\textwidth}  % tweaks the width, makes a new \textwidth
				\parbox[t][\columnheight]{\textwidth}{ % must be some better way to set the the height, width and textwidth simultaneously
					\begin{myblock}{Abstract}
					    BehavioPy (\href{https://github.com/TheChymera/behaviopy}{github.com/TheChymera/behaviopy}) is a novel software package providing evaluation, analysis, and plotting functionalities for behavioural and pyhsiological data, including simple parameterized one-function solutions for common use cases.
						Current data evaluation capabilities consist in flexible event tracking for video recordings, while analysis functionalities center around correlation matrices, significance testing, and multiple comparison correction.
						Plotting can be performed based on Pandas DataFrame objects or comma separated values files and is highly configurable. 

						\vspace{0.8em}
	
						BehavioPy addresses the need for easy-to-use, reproducible, and automatable high-level analysis interfaces in behavioural and physiological research.
						It brings the power of the scientific Python stack to fields still highly reliant on proprietary solutions, and serves as a repository for the complex and heterogeneous boilerplate code required to make multiple types of scientific Python plots consistent in appearance.
					\end{myblock}\vfill
					\vspace{-0.3em}
					\begin{myblock}{Issues and Solution Concept}
						Behavioural and physiological data are commonly processed in heterogeneous software environments, giving rise to convoluted workflows
						--- as exemplified in figure~\ref{fig:wf_trad}.
						Numerous factors contribute to such problematic configurations, including:
						\begin{itemize}
							\item Input/output (I/O) incompatibility between different (proprietary) software packages.
							\item Restricted application programming interfaces (APIs) preventing data piping.
							\item Closed source, preventing feature addition to existing workflow components. 
						\end{itemize}
                                                
						\vspace{0.4em}
						The consequences thereof for research performance are similarly extensive, including:
						\begin{itemize}
							\item High time cost for workflow execution.
							\item Limited reproducibility of the analysis environment and workflow execution.
							\item Ample opportunity for data deterioration or manipulation.
						\end{itemize}
                                                \begin{figure}
                                                        \centering
							\vspace{-3em}
							\subfloat[\scriptsize\textcolor{ETH6}{
								Fictitious workflow illustrating issues which commonly affect behavioural and/or physiological data processing.
								Note the high centrality of the "Reformat" node - a highly prominent process whenever I/O and API functionality is restricted, and which relies primarily on manual data editing.
								Note also that each individual functionality may be performed by a separate package - this contributes to I/O and API issues.
								}\label{fig:wf_trad}]{\includedot[scale=1.7]{data/wf_trad}}\qquad
							\vspace{-2em}
							
							\subfloat[\scriptsize\textcolor{ETH6}{
								Conceptual workflow showing how BehavioPy uses piping to simplify, speed up, and protect behavioural and physiological data processing from exposure to manual editing.
								Note that workflow flexibility is not compromised by piping, as data reports can be introduced at any step of the BehavioPy pipeline --- at the evaluation stage (e.g. for \cref{fig:ftt}), at the analysis stage (e.g. for \cref{fig:pbr}), or at the plotting stage (e.g. for \cref{fig:tt}).
								}\label{fig:wf_bp}]{\includedot[scale=1.6]{data/wf_bp}}\qquad
                                                	\vspace{0.4em}
							\caption{
								Directed graphs depicting paths from raw behavioural and physiological data to reports and plots. 
								Folder-shaped nodes indicate common information archiving stages.
								Diamond-shaped nodes indicate highly automatized steps, whereas increasingly polygonal shapes indicate steps requiring more manual intervention 
								--- rounded shapes indicate fully manual steps.
								Colored outlines indicate correspondence to a particular software package.
								\label{fig:wf}}
                                                \end{figure}
                                                \vspace{0.2em}
					
						We use the scientific Python stack to create a cohesive and extendable open source environment for behavioural and physiological data processing (as shown in figure~\ref{fig:wf_bp}).
						BehavioPy input nodes can be seamlessly linked to from LabbookDB \cite{ldb} --- or any other library supporting configurable comma separated values (CSV) or Pandas DataFrame \cite{pandas} outputs.

					\end{myblock}\vfill
					\vspace{-0.3em}
					\begin{myblock}{Input Format}
                                                \vspace{0.4em}
						\py{pytex_tab('scripts/sp_table.py', label="sp", caption="Example of sucrose preference test \cite{Eagle2016} data input compatible with BehavioPy.", options="\scriptsize")}
						\vspace{0.4em}
						
						\begin{minipage}{.52\textwidth}
						    BehavioPy accepts “long form” (providing one and only one dedicated value column) input data (as seen in table~\ref{tab:sp}) either as piped Pandas DataFrame objects, or CSV files stored on disk.
							The desired plot configuration is selected by passing appropriate parameters to general-purpose BehavioPy functions.
							An example of this can be seen in figure~\ref{fig:code}, which contains example code able to produce \cref{fig:spt} from table~\ref{tab:sp}).
						\end{minipage}
						\begin{minipage}{.02\textwidth}~\end{minipage}
						\begin{minipage}{.42\textwidth}
							\begin{figure}
								\fcolorbox{vlg}{elg}{\spcode}
								\caption{BehavioPy code snipped for plotting sucrose preference test data for two treatment categories. The \texttt{df} variable represents either a Pandas DataFrame or a path. \label{fig:code}}
							\end{figure}
						\end{minipage}
					\end{myblock}\vfill
					\vspace{-0.3em}
					\begin{myblock}{Outlook}
						\begin{itemize}
							\item Enforce \cref{fig:wf_bp} concept by disambiguating LabbookDB and BehavioPy reporting.
							\item Implement more automated behavioural data evaluation.
							\item Further develop physiological (water consumption and weight) tracking capabilities.
							\item Improve code documentation.
						\end{itemize}
					\end{myblock}\vfill
		}\end{minipage}\end{beamercolorbox}
	\end{column}
	\begin{column}{.6\textwidth}
		\begin{beamercolorbox}[center]{postercolumn}
			\begin{minipage}{.98\textwidth} % tweaks the width, makes a new \textwidth
				\parbox[t][\columnheight]{\textwidth}{ % must be some better way to set the the height, width and textwidth simultaneously
					\begin{myblock}{Evaluation}
						\begin{center}
							\begin{minipage}{.37\textwidth}
							\vspace{1em}
								\begin{figure}
									\centering\includegraphics[width=\textwidth]{img/eval.png}
									\caption{Screenshot of Forced Swim Test \cite{Can2012} video recording evaluation, as displayed by BehavioPy. \label{fig:eval}}
								\end{figure}
							\end{minipage}
							\begin{minipage}{.02\textwidth}~\end{minipage}
							\begin{minipage}{.58\textwidth}
								\vspace{-0.5em}
								Behavioural data analysis often relies on manual evaluation of video recordings (e.g. in the form of event tracking).
								While we maintain an outlook towards more automated evaluation methods (including via machine learning \cite{Valletta2017}), these are still difficult to implement in a general-purpose fashion.
								
								\vspace{0.5em}	

								Currently we provide an easily configurable general-purpose event tracking interface which can record as many event types as there are usable input keys.
								This interface leverages PsychoPy \cite{Peirce2007} functionalities, allowing sub-millisecond response recording precision, and full display blanking save for the recording bracket --- as shown in figure~\ref{fig:eval}. 
							\end{minipage}
						\end{center}
					\end{myblock}\vfill
					\begin{myblock}{Significance Testing}
						One of the most common types of analysis for behavioural data is significance testing.
						The results of this analysis frequently feature in scientific plots, yet notably both Matplotlib \cite{matplotlib} (the main plotting utility of the scientific Python stack) and Seaborn \cite{seaborn} (a higher level plotting utility leveraging the features of Matplotlib) omit convenient switches for significance annotation.

						\vspace{0.8em}
						
						BehavioPy can perform the simple underlying computation via the SciPy library \cite{scipy} and automatically insert the significance value into Matplotlib plots - all while maintaining coherence with the plot's style specifications.
						In \cref{fig:ftt,fig:spt,fig:sps} we present this feature applied to common use cases from behavioural data analysis, represented as swarm plots --- a style which facilitates qualitative inspection of small sample size distributions.
						
						\vspace{0.7em}
						\begin{minipage}{.31\textwidth}
						\py{pytex_fig('scripts/forcedswim_ttest.py', conf='poster/swarmplots.conf', label='ftt', caption='Immobility ratio comparison over two recommended time frames \cite{Costa2013} of interest in the forced swim test. This figure represents an instance of data being piped through all the BehavioPy functionalities (reporting, analysis, and plotting --- as seen in \cref{fig:wf_bp}).')}
						\end{minipage}
						\begin{minipage}{.015\textwidth}~\end{minipage}
						\begin{minipage}{.31\textwidth}
						    \py{pytex_fig('scripts/sucrosepreference_treatment.py', conf='poster/swarmplots.conf', label='spt', caption='Sucrose preference comparisons (significance levels uncorrected) of treatment groups during discrete time periods. This plot is produced by running the code in \cref{fig:code} with the data in \cref{tab:sp} as input.')}
						\end{minipage}
						\begin{minipage}{.015\textwidth}~\end{minipage}
						\begin{minipage}{.31\textwidth}
						    \py{pytex_fig('scripts/sucrosepreference_side.py', conf='poster/swarmplots.conf', label='sps', caption='Sucrose preference comparisons (significance levels uncorrected) of bottle locations in each testing enclosure (corresponding to one and only one animal each). These comparisons are relevant for sucrose preference data quality control..')}
						\end{minipage}
					\end{myblock}\vfill
					\begin{myblock}{Timetable Plot}
						The timetable plot exemplified in \cref{fig:tt} is a functionality unique to BehavioPy, created to allow detailed overview and reporting of cohorts --- resolved at the animal level.
						The corresponding plotting function is highly configurable, as well as seamlessly compatible with LabbookDB-formatted reports.
						Timetable plotting is an example of external data reports being used directly with BehavioPy plotting functions (i.e. at the very end of the BehavioPy pipeline --- indicated as a possibile usage scenario in \cref{fig:wf_bp}). 
						\vspace{0.5em}
						\py{pytex_fig('scripts/timetable.py', conf='poster/timetable.conf', label='tt', caption='Animal cohort \cite{ismrm2017} treatment and measurement timetable. Functional magnetic  resonance imaging dates are shaded dark, chronic (d.w.) fluoxetine administration days tinted yellow, acute (i.v.) fluoxetine administration days dotted orange, and vehicle administration dotted blue. Green dots mark open field test days, and black dots forced swim test days.')}
			
					\end{myblock}\vfill
					\begin{myblock}{Correlation Matrices}
						\vspace{1em}
						
						\begin{minipage}{.31\textwidth}
						    \py{pytex_fig('scripts/petbehaviour_r.py', conf='poster/matrices.conf', label='pbr', caption='Correlation matrix for behaviour and positron emission tomography data - with rows corresponding to brain region of interests and columns to behaviour types. Please see the original study \cite{Herde2017} for the abbreviation legend.')}
						\end{minipage}
						\begin{minipage}{.02\textwidth}~\end{minipage}
						\begin{minipage}{.31\textwidth}
						    \py{pytex_fig('scripts/petbehaviour_p.py', conf='poster/matrices.conf', label='pbp', caption='Uncorrected significance levels of the correlation analysis presented in \cref{fig:pbr} --- colored with a nonuniform heat map designed to emphasize p-values below a 0.05 threshold. As the data set has no missing entries, p and r values are proportional.')}
						\end{minipage}
						\begin{minipage}{.02\textwidth}~\end{minipage}
						\begin{minipage}{.31\textwidth}
						    \py{pytex_fig('scripts/petbehaviour_pcorrected.py', conf='poster/matrices.conf', label='pbp_c', caption='Significance levels of the correlation analysis presented in \cref{fig:pbr}, corrected for a false discovery rate of 0.05 accoding to the Benjamini-Hochberg procedure --- and colored with the same nonuniform heat map as \cref{fig:pbp}.')}
						\end{minipage}
						
						\vspace{0.5em}

						BehavioPy can also compute correlation matrices of behavioural data and data from other modalities (e.g. physiology or imaging).
						\Cref{fig:pbr,fig:pbp,fig:pbp_c} depict correlation and significance metrics for the relationship between cumulative behavior duration and radioligand binding potential in brain regions of interest --- based on data from a mouse behaviour and positrion emission tomography study \cite{Herde2017}.
						
						\vspace{0.8em}

						The above figures demonstrate how data reports generated by separate packages (in this case not LabbookDB) can be introduced directly at the analysis stage of the BehavioPy pipeline --- indicated as a possibile usage scenario in \cref{fig:wf_bp}.
					\end{myblock}\vfill
                                        \begin{myblock}{References}
                                                \vspace{-0.8em}
                                                \begin{multicols}{3}
                                                        \tiny
                                                        \bibliographystyle{unsrt}
                                                        \bibliography{./bib}
                                                \end{multicols}
                                        \end{myblock}\vfill
		}\end{minipage}\end{beamercolorbox}
	\end{column}
\end{columns}
\scriptsize{This is an open source reproducible document. Download the source code from: \href{https://bitbucket.org/TheChymera/behaviopy_repsep}{bitbucket.org/TheChymera/behaviopy\_repsep}}
\end{frame}
\end{document}
