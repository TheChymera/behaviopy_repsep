from os import path

from behaviopy.analysis import correlation_matrix
from behaviopy.abbreviations import *

data_dir = "data/herde2017"
radioligand_df_path = path.join(data_dir,"SUV_con40-60.csv")
behavior_df_path = path.join(data_dir,"DONOR.csv")

rois = [
	"amygdala",
	"bfs", 
	"brain stem",
	"striatum",
	"cerebellum",
	"central gyrus",
	"inferior colliculus",
	"superior colliculus",
	"cortex", "hippocampus",
	"hypothalamus",
	"midbrain",
	"olfactory",
	"thalamus",
	]
behaviors = [
	"assisted rearing",
	"unassisted rearing",
	"wall walking",
	"center walking",
	"object interaction",
	"body grooming",
	"head grooming",
	"risk assessment",
	"immobility",
	]
animals = [
	"t1",
	"t2",
	"t3",
	"t4",
	"t5",
	]

correlation_matrix(behavior_df_path,
	df_y_path=radioligand_df_path,
	y_cols=rois,
	x_cols=behaviors,
	y_dict=ROI_ABBREV,
	x_dict=AGGRESSIVE_BEHAVIOUR_ABBREV,
	entries=animals,
	output="pearsonr",
	bp_style=False,
	)
