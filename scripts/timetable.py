import pandas as pd
from behaviopy import plotting

df_path = "data/timetable.csv"
df = pd.read_csv(df_path)

shade = ["FMRIMeasurement_date"]
draw = [
	"ForcedSwimTestMeasurement_date",
	{"TreatmentProtocol_code":["aFluIV","Treatment_start_date"]},
	{"TreatmentProtocol_code":["aFluIV_","Treatment_start_date"]},
	"OpenFieldTestMeasurement_date",
	]
saturate = [
	{"Cage_TreatmentProtocol_code":["cFluDW","Cage_Treatment_start_date","Cage_Treatment_end_date"]},
	{"Cage_TreatmentProtocol_code":["cFluDW_","Cage_Treatment_start_date","Cage_Treatment_end_date"]},
	{"TreatmentProtocol_code":["aFluSC","Treatment_start_date"]},
	]

plotting.timetable(df, "Animal_id",
	draw=draw,
	saturate=saturate,
	shade=shade,
	window_end="2017,3,21",
	bp_style=False,
	)

