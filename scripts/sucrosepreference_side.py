from os import path

from behaviopy.plotting import expandable_ttest

expandable_ttest("data/sucrosepreference.csv",
	colorset=["r","g"],
	compare="Sucrose Bottle Position",
	comparisons={"Cage ID":[]},
	datacolumn_label="Sucrose Preference Ratio",
	rename_treatments={"cFluDW":"Fluoxetine","cFluDW_":"Control"},
	save_as="sucrosepreference_side.pdf",
	bp_style=False,
	)

