from behaviopy.plotting import expandable_ttest

expandable_ttest("data/sucrosepreference.csv",
        compare="Treatment",
        comparisons={"Period [days]":[]},
        datacolumn_label="Sucrose Preference Ratio",
        rename_treatments={
                "cFluDW":"Fluoxetine",
                "cFluDW_":"Control"
                },
        bp_style=False,
        )
