from behaviopy.plotting import expandable_ttest

expandable_ttest("data/forcedswim_bins.csv",
	compare="Treatment",
	comparisons={"Interval [minutes]":[]},
	datacolumn_label="Immobility Ratio",
	rename_treatments={"cFluDW":"Fluoxetine","cFluDW_":"Control"},
	bp_style=False,
	)

